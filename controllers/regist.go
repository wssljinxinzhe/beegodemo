package controllers

import (
	"github.com/astaxie/beego"
)

type Regist struct {
	beego.Controller
}

func (c *Regist) Get() {
	c.TplName = "regist.html"
}
