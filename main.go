package main

import (
	_ "beegodemo/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

