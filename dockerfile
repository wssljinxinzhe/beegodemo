FROM golang
RUN mkdir -p /go/src/beegodemo
WORKDIR /go/src/beegodemo

COPY . /go/src/beegodemo

RUN go get github.com/astaxie/beego && go get github.com/beego/bee

EXPOSE 8080

CMD ["bee","run"]
